from django.contrib import admin
from django.urls import path, include
from userAPP import views
from rest_framework.routers import DefaultRouter
import userAPP

# Creating Router object
router = DefaultRouter()

# Register StudentViewSet with Router

router.register('api/user_list',views.UserModelViewSet,basename='user')
router.register('api/single_user_list',views.SingleUserModelViewSet,basename='singleuser')



urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include(router.urls)),
    path('',include('userAPP.urls'))
]