### Django Rest framework 

Created a superadmin.

## Adding Data

Added users from the superadmin page and assigned them staff, non-staff users status by checkbox.


### Fetching single userdata

Single user can be fetched from following end point, By username filter at the end. This route can be accessed by anyone (No authentication is needed).



```bash
Endpoint

http://127.0.0.1:8000/api/single_user_list/?username=superuser

## JSON Results

HTTP 200 OK
Allow: GET, POST, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

[
    {
        "username": "superuser",
        "first_name": "Shyam",
        "last_name": "Kumar",
        "email": "Shyam@gmail.com",
        "groups": [],
        "user_permissions": [],
        "is_staff": true,
        "is_active": true,
        "is_superuser": true,
        "last_login": "2021-12-15T01:55:38Z",
        "date_joined": "2021-12-14T17:19:32Z"
    }
]
```

### Fetching All userdata

All user's can be fetched from following end point, This route can be accessed by staff-users and superuser only (Authentication is needed).



```bash
Endpoint

GET /api/user_list/

## JSON Results

HTTP 200 OK
Allow: GET, POST, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

[
    {
        "username": "superuser",
        "first_name": "Shyam",
        "last_name": "Kumar",
        "email": "Shyam@gmail.com",
        "groups": [],
        "user_permissions": [],
        "is_staff": true,
        "is_active": true,
        "is_superuser": true,
        "last_login": "2021-12-15T01:55:38Z",
        "date_joined": "2021-12-14T17:19:32Z"
    },
    {
        "username": "admin",
        "first_name": "Sohail",
        "last_name": "Sheik",
        "email": "Sohail@gmail.com",
        "groups": [],
        "user_permissions": [],
        "is_staff": true,
        "is_active": true,
        "is_superuser": false,
        "last_login": null,
        "date_joined": "2021-12-14T17:21:04Z"
    },
    {
        "username": "admin2",
        "first_name": "Narendra",
        "last_name": "babu",
        "email": "narendra@gmail.com",
        "groups": [],
        "user_permissions": [],
        "is_staff": false,
        "is_active": true,
        "is_superuser": false,
        "last_login": null,
        "date_joined": "2021-12-15T01:59:18Z"
    }
]
```
